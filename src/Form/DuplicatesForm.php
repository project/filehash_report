<?php

namespace Drupal\filehash_report\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\HTML;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Filehash report form.
 */
class DuplicatesForm extends FormBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The name of the table that has the sha256 column.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $hashtable;

  /**
   * @param \Drupal\Core\Database\Connection $database
   *  The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
    $this->hashtable = $this->getHashTableName();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'filehash_report_duplicates';
  }


  /**
   *  Return sha256 for fid.
   **/
  public function getHashByFid($fid) {
    $result = $this->database->query('select sha256 from {' . $this->hashtable . '} where fid = :fid', [ ':fid' => $fid ])
      ->fetchAllAssoc('sha256', \PDO::FETCH_ASSOC);
    $sha256s = array_keys($result);
    $sha256 = reset($sha256s);
    return $sha256;
  }

  /**
   *  Return all fids for sha256 value.
   **/
  public function getFidsBySha256($sha256) {
    $result = $this->database->query('select fid from {' . $this->hashtable . '} where sha256 = :sha256', [ ':sha256' => $sha256 ])
      ->fetchAllAssoc('fid', \PDO::FETCH_ASSOC);
    $fids = array_keys($result);
    return $fids;
  }

  /**
   *  Return array of fid and sha256 values with the fid as the array key.
   **/
  public function getDuplicateRecords() {
    $result = $this->database->query('select fid, sha256 from {' . $this->hashtable . '} where sha256 in (select sha256 from {' . $this->hashtable . '} group by sha256 having count(sha256) > 1) order by sha256')
    ->fetchAllAssoc('fid', \PDO::FETCH_ASSOC);
    return $result;
  }

  public function buildHtmlTable() {
    $base_path = Url::fromRoute('<front>')->toString();
    $report_path = Url::fromRoute('filehash.reportduplicates')->toString();
    $filehash_path = Url::fromRoute('filehash.admin')->toString();
    $table_html = '';
    $empty_html;
    $duplicates = [];
    $error_html = '<h2>' . t('An error has occured') . '</h2>' ;
    $error_html .= '<p>' . t('sha256 values are not present in the database.') . '</p>';
    $error_html .= '<p>' . t('Please review settings for filehash.') .  ' <a href="' . $filehash_path . '">' . t('Filehash settings') . '</a>' . '</p>';
    if ($this->hashtable == 'bogus') {
      return $error_html;
    }
    try {
      $duplicates = $this->getDuplicateRecords();
    }
    catch (Exception $e) {
      $duplicates = [];
      $table_html = '<h2>' . t('An error has occured') . '</h2>' ;
      $table_html .= '<p>' . $e->getMessage() . '</p>';
      $table_html .= '<p>&nbsp;</p>';
      $table_html .= '<p>' . t('Please review settings for filehash.') .  ' <a href="' . $filehash_path . '">' . t('Filehash settings') . '</a>' . '</p>';
      return $table_html;
    }
    $table_html = '<table class="file-hash table">';
    $table_html .= '<thead>';
    $table_html .= '<td>' . t('FID action') . '</td>';
    $table_html .= '<td>' . t('FID usage') . '</td>';
    $table_html .= '<td>' . t('File name') . '</td>';
    $table_html .= '<td>' . t('File uri') . '</td>';
    $table_html .= '<td>' . t('Sha256') . '</td>';
    $table_html .= '</thead>';
    foreach ($duplicates as $dupe) {
      $fid_matched = $dupe['fid'];
      $sha256 = $dupe['sha256'];
        $file = \Drupal\file\Entity\File::load($fid_matched);
        if (!$file) {
          continue;
        }
        $table_html .= '<tr>';
        $table_html .= '<td>';
        $table_html .= "<a href='" . $base_path . "/admin/content/files/replace/$fid_matched?destination=$report_path'>" . $fid_matched . "</a><br>";
        $table_html .= '</td>';
        $table_html .= '<td>';
        $table_html .= "<a href='" . $base_path . "/admin/content/files/usage/$fid_matched?destination=$report_path' target='_blank'>" . $fid_matched . "</a><br>";
        $table_html .= '</td>';
        $filename = $file->getFilename();
        $table_html .= '<td>';
        $limited_filename = HTML::escape($filename);
        $limited_filename = substr($filename, 0 , 70);
        $table_html .= $limited_filename;
        $table_html .= '</td>';
        $file_uri = str_replace('public://', '', $file->getFileUri());
        $file_uri = (string) HTML::escape($file_uri);
        $table_html .= '<td>';
        $table_html .= "<a href='" . $base_path . '/../sites/default/files/' . $file_uri . "'>" . substr($file_uri, 0, 35) . "</a>";
        $table_html .= '</td>';
        $table_html .= '<td>';
        $table_html .= substr($sha256, 0, 8) . '...';
        $table_html .= '</td>';
        $table_html .= '</tr>';
    }
    $table_html .= '</table>';
    $table_html .= '<div>' . count($duplicates) . ' ' . t('duplicates in total') . '</div>';
    if (empty($duplicates)) {
      return $error_html;
    }
    return $table_html;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $args = [];
    $html = $this->buildHtmlTable();
    $html_formatted = new FormattableMarkup($html, $args);
    $form['message'] = [
      '#type' => 'markup',
      '#title' => $this->t('Message'),
      '#markup' => $html_formatted,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Re-generate file hashes'),
    ];

    return $form;
  }

  public function getHashTableName() {
    $tablename = 'bogus';
    if ($this->database->schema()->tableExists('filehash') &&
    $this->database->schema()->fieldExists('filehash', 'sha256')) {
      $tablename = 'filehash';
    }
    else if ($this->database->schema()->tableExists('file_managed') &&
    $this->database->schema()->fieldExists('file_managed', 'sha256')) {
      $tablename = 'file_managed';
    }
    return $tablename;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
//    if (mb_strlen($form_state->getValue('message')) < 10) {
//      $form_state->setErrorByName('message', $this->t('Message should be at least 10 characters.'));
//    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $report_path = Url::fromRoute('filehash.reportduplicates')->toString();
    $generate_path = Url::fromRoute('filehash.generate')->toString();
    $form_state->setRedirectUrl(Url::fromUserInput($generate_path . '?destination=' . $report_path));
    $this->messenger()->addWarning($this->t('Please click the generate button to generate the required sha-256 hashes (if those are enabled).'));
  }

}
